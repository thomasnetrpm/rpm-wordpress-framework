<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<!--Site Content-->
	<section class="site-content two-column" role="main">
	    <div class="inner-wrap">
	        <article class="site-content-primary">
				<p class="post-meta">
					Posted by <?php the_author_link(); ?> on <time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> | <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?> 
				</p>
				
				<div class="text-aligncenter">
				<?php if(has_post_thumbnail()): ?>
				<?php $title= get_the_title(); ?>
				<a href="<?php the_post_thumbnail_url(); ?>" class="lightbox"><?php the_post_thumbnail('large',array('alt' =>$title, 'title' =>$title)); ?></a>
				<?php endif; ?>	
				</div>

				<?php the_content(); ?> 

				<!-- Post Navigation -->
				<?php
				if ( is_singular( 'post' ) ) {
				    // Previous/next post navigation.
				    the_post_navigation(
				        array(
				            'next_text' => '<span class="next-post">' . __( 'Next »', 'nd_dosth' ) . '</span> ' .
				                '<span class="post-title">%title</span>',
				            'prev_text' => '<span class="previous-post">' . __( '« Previous', 'nd_dosth' ) . '</span> ' .
				                '<span class="post-title">%title</span>',
				        )
				    );
				}
				?>
				<!-- Post Navigation -->
				<!-- <hr>
				<p><?php the_tags(); ?></p>
				<hr> -->
				<?php comments_template( '', true ); ?>
	        </article>
	       	<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/sidebar-blog','parts/shared/flexible-content'  ) ); ?>
	    </div>
	</section>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>