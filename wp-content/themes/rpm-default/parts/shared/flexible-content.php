
<?php if( have_rows('flexible_content') ): echo '<section class="additional-content">';
    while ( have_rows('flexible_content') ) : the_row(); ?>

	<?php if( get_row_layout() == 'tab_content' ): ?>
		<?php if( get_sub_field('fullwidth') == false): ?>
			<section class="accordian-tabs-module">
			 	<div class="inner-wrap">		 	
			 		<?php if( get_sub_field('section_header')): ?>
						<h2><?php echo get_sub_field('section_header'); ?></h2>
					<?php endif; ?>
					<?php if( get_sub_field('section_subtext')): ?>
						<p class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></p>
					<?php endif; ?>

					<ul class="accordion-tabs">
						<?php if( have_rows('tab_content_row') ): while ( have_rows('tab_content_row') ) : the_row(); ?>
							<li class="tab-header-and-content">
								<a href="javascript:void(0)" class="tab-link"><?php echo get_sub_field('tab_header'); ?></a>
								<div class="tab-content"><p><?php echo get_sub_field('tab_body'); ?></p></div>							
							</li>
						<?php endwhile; ?>
						<?php endif; ?>
					</ul>
					<?php if( get_sub_field('divider')): ?>
						<hr>
					<?php endif; ?>			
				</div>
			</section>
		<?php endif; ?>

	<?php elseif( get_row_layout() == 'full_width_cta' ): ?>

		<section class="full-width-cta-test">
			<div class="inner-wrap"><h2 class="cta-banner-header"><?php echo get_sub_field('section_header'); ?></h2></div>
			<section class="fwc-module">
				<div class="inner-wrap">		
					<div class="row cta-banner bottom-baseline">
			            <p class="h2 cta-banner-body"><?php echo get_sub_field('section_body'); ?></p>	
			       		<?php
						$cta_button = get_sub_field('cta_button');
						if($cta_button):
						$link_url = $cta_button['url'];
						$link_title = $cta_button['title'];
						$link_target = $cta_button['target'] ? $cta_button['target'] : '_self';
						?>
						<a class="btn fw-cta" href="<?php echo esc_url($link_url); ?>"><span><?php echo esc_html($link_title); ?></span></a>
						<?php endif; ?> 
			        </div>
				</div>
			</section>			
			<?php if( get_sub_field('divider')): ?>
				<div class="inner-wrap"><hr></div>
			<?php endif; ?>
		</section>		
        

 	<?php elseif( get_row_layout() == 'multiple_columns' ): ?>
 		<section class="multiple-cols-module" <?php if( get_sub_field('bg_color')): ?>style="background-color:<?php echo get_sub_field('bg_color'); ?>"<?php endif; ?> <?php if( get_sub_field('id')): ?> id="<?php echo get_sub_field('id'); ?>"<?php endif; ?>>
		 	<div class="inner-wrap">	
		 		<?php if( get_sub_field('section_header')): ?>
					<h2><?php echo get_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></p>
				<?php endif; ?>
				<section class="<?php if (get_sub_field('number_columns') == '2') {
						echo 'rows-of-2';
					} else if (get_sub_field('number_columns') == '3') {
					        echo 'rows-of-3';
					} else if (get_sub_field('number_columns') == '4') {
					        echo 'rows-of-4';
					}
					?> <?php if( get_sub_field('new_class')): ?><?php echo get_sub_field('new_class'); ?><?php endif; ?>">

		         	<?php if( have_rows('content') ): while ( have_rows('content') ) : the_row(); ?>
						<div><?php echo get_sub_field('content_column'); ?></div>
					<?php endwhile; ?>
					<?php endif; ?>				
				</section>
				<?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
 		</section>	

	<?php elseif( get_row_layout() == 'img_gallery_section' ): ?>
		<?php if( get_sub_field('fullwidth') == false): ?>
			<section class="image-gallery-module">
				<div class="inner-wrap">	
					<?php if( get_sub_field('section_header')): ?>
						<h2><?php echo get_sub_field('section_header'); ?></h2>
					<?php endif; ?>
					<section class="popup-gallery <?php if (get_sub_field('number_columns') == '2') {
								echo 'rows-of-2';
							} else if (get_sub_field('number_columns') == '3') {
							        echo 'rows-of-3';
							} else if (get_sub_field('number_columns') == '4') {
							        echo 'rows-of-4';
							}
							?>">
						<?php $images = get_sub_field('img_gallery');
							if( $images ): ?>
								<?php foreach( $images as $image ): ?>
			                    	<a href="<?php echo $image['sizes']['large']; ?>" class=" loop-item">
				                    	<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>"/>
			                    		<h3 class="li-title"><?php echo $image['caption']; ?></h3>
			                    	</a>
								<?php endforeach; ?>
							<?php endif; ?>
					</section>
					<?php if( get_sub_field('divider')): ?>
							<hr>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
		
		
			
	<?php elseif( get_row_layout() == 'img_gallery_with_thumbnails' ): ?>
    <section class="image-gallery-with-thumbs">
          <div class="inner-wrap"><?php if( get_sub_field('imt_section_header')): ?>
            <h2><span><?php echo get_sub_field('imt_section_header'); ?></span></h2><?php endif; ?>
            
            <?php $images = get_sub_field('imgwt_gallery');
              if( $images ): ?>
            <div class="innerpage-carousel">
              <div id="slider"  class="slides slider-for popup-gallery">
                <?php foreach( $images as $image ): ?>
                <div class="item">
                  <a href="<?php echo $image['sizes']['large']; ?>">
                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>"/>
                  </a>
                </div>  
                 <?php endforeach; ?>
              </div>
              <div id="carousel" class="slides slider-nav">
                 <?php foreach( $images as $image ): ?>
                <div class="item">
                  <a href="javascript:void(0)" class="slider-nav-item">
                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>"/>
                  </a>
                </div>
                <?php endforeach; ?>        
              </div>           
            </div> <?php endif; ?>
           <?php if( get_sub_field('divider')): ?>
        <hr>
      <?php endif; ?>
      </div>
    </section>


	<?php elseif( get_row_layout() == 'image_gallery_without_thumbnails' ): ?>
		<?php if( get_sub_field('wim_slider')): ?>

			<?php $img = get_sub_field('wim_slider');
			if( $img ): ?>	
				<div class="inner-wrap">
			       	<section class="innerpage-carousel-widthout-thumb">
			       		<div class="icwt-slider popup-gallery">
							<?php foreach( $img as $image ): ?>
			                <div class="item">
			                  	<a href="<?php echo $image['sizes']['large']; ?>">
									<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php $image['alt']; ?>" title="<?php $image['alt']; ?>"/>
								</a>
			                </div> 
			 				<?php endforeach; ?>  
	              		</div>
	         		</section>
              		<?php if( get_sub_field('divider')): ?>
						<hr>
					<?php endif; ?>
	 		 	</div>
			<?php endif; ?>
		<?php endif; ?>


	<?php elseif( get_row_layout() == 'click_expand' ): ?>
		<?php if( get_sub_field('fullwidth') == false): ?>
			<section class="click-expand-module">
				<div class="inner-wrap">
					<div class="click-expand <?php if( get_sub_field('spacing')): ?>spacing-bottom<?php endif; ?>">
			          <h3 class="ce-header" tabindex="0"><?php echo get_sub_field('section_header'); ?></h3>
			          <div class="ce-body"><?php echo get_sub_field('section_body'); ?></div>
			      	</div>
			    </div>
			</section>	        
		<?php endif; ?>

 			
	<?php elseif( get_row_layout() == 'table' ): ?>
		<section class="tabular-data">
		   <div class="inner-wrap">
		        <?php if( get_sub_field('section_header')): ?>
		            <div class="headexpand-wrap">  
		            	<h2 class="headexpand"><?php echo get_sub_field('section_header'); ?></h2>
				<?php endif; ?>
							<?php if( get_sub_field('section_header')): ?>
								<h3 class="column-subtext"><?php echo get_sub_field('section_subtext'); ?></h3>
							<?php endif; ?>
					        <?php if( get_sub_field('table_content')): ?>
					            <div class="table-wrap">
					                <table class="tablesaw tablesaw-stack" data-tablesaw-mode="stack">
					                	<?php echo get_sub_field('table_content'); ?>
					                </table>
					            </div>
					        <?php endif; ?>
					        <?php if( get_sub_field('section_header')): ?>
		           </div> 
		           <!--headexpand-wrap END -->
		        <?php endif; ?>

		        <?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
		</section>	


	<?php elseif( get_row_layout() == 'product_grid' ): ?>
		<section class="product-grid-module">
			<div class="inner-wrap">
				<?php if( get_sub_field('section_header')): ?>
					<h2 class="carousel-header"><?php echo get_sub_field('section_header'); ?></h2>
				<?php endif; ?>
				<?php if( get_sub_field('section_subtext')): ?>
					<p><?php echo get_sub_field('section_subtext'); ?></p>
				<?php endif; ?>

				<div class="product-item-wap">
					<div class="product-items">
						<?php if( have_rows('product_row') ): while ( have_rows('product_row') ) : the_row(); ?>
							<div>
								<?php 	
																	
									$link = get_sub_field('product_url');
									if( $link ): 
										$link_url = $link['url'];
										$link_title = $link['title'];
										?>
										<a class="product-item" href="<?php echo esc_url($link_url); ?>"> 
											<span class="product-img">
												<?php if(get_sub_field('product_picture')) : ?>
													<?php $product_picture = get_sub_field('product_picture'); ?>
													   <img class="pmi-img" src="<?php echo $product_picture['sizes']['product_thumb']; ?>" alt="<?php echo $product_picture['title']; ?>" title="<?php echo $product_picture['title']; ?>">
												<?php endif; ?>
											</span>									
											<span class="product-title"><?php echo esc_html($link_title); ?></span>
										</a>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
							<?php endif; ?>
					</div>

				</div>


				<div class="<?php if( get_sub_field('carousel')): ?>flexslider<?php endif; ?> product-carousel">
					<ul class="slides">
						<?php if( have_rows('product_row') ): while ( have_rows('product_row') ) : the_row(); ?>
							<li>
								
							</li>
						<?php endwhile; ?>
						<?php endif; ?>
					</ul>
				</div>
				<?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
		</section>

	<?php elseif( get_row_layout() == 'text_media' ): ?>
		<section class="text-media-module">
			<div class="inner-wrap">
				<?php if( get_sub_field('section_header')): ?>
					<h2><?php echo get_sub_field('section_header'); ?></h2>
				<?php endif; ?>			

				<div class="rows-of-2">
	              <div>
	                <?php echo get_sub_field('media'); ?>
	              </div>
	              <div>
	                <?php echo get_sub_field('text'); ?>
	              </div>
	            </div>

				<?php if( get_sub_field('divider')): ?>
					<hr>
				<?php endif; ?>
			</div>
		</section>

	<?php elseif( get_row_layout() == 'heading_wrap' ): ?>
		<!-- Headign with BG -->
		<div class="heading-wrap">
			<div class="inner-wrap">
				<div class="rows-of-2">
				    <div class="on-light-bg">
					    <div class="hw-txt">
					    	<h1 class="lb-title"><?php echo get_sub_field('lh_heading'); ?></h1>
					        <h2><?php echo get_sub_field('lh_sub_heading'); ?></h2>
					        <p><?php echo get_sub_field('lh_intro_text'); ?></p>
					    </div>
				    </div>
				    <div class="on-color-bg">
					    <div class="hw-txt">                
					        <h1><?php echo get_sub_field('right_heading'); ?></h1>
					        <h2><?php echo get_sub_field('right_subheading'); ?></h2>
					        <p><?php echo get_sub_field('right_intro_text'); ?></p>
					    </div>
				    </div>
				</div>
			</div>
		</div>
		<!-- Headign with BG -->

	<?php elseif( get_row_layout() == 'image_content_grid_module' ): ?>
		<section class="image-content-grid-module">
			<div class="inner-wrap">
				<?php if (get_sub_field('icgm_section_heading')): ?>
					<h2 class="icgm-section-heading"><?php echo get_sub_field('icgm_section_heading') ?></h2>
				<?php endif ?>

				<?php if (get_sub_field('icgm_section_description')): ?>
					<p class="icgm-section-description"><?php echo get_sub_field('icgm_section_description') ?></p>
				<?php endif ?>

				<?php if (get_sub_field('number_of_columns') == '2') {
					$gridClass = 'tse-cols-2';
				} else if (get_sub_field('number_of_columns') == '3') {
				        $gridClass = 'tse-cols-3';
				} else if (get_sub_field('number_of_columns') == '4') {
				        $gridClass = 'tse-cols-4';
				} else {
					$gridClass = '';
				}
				?>

				<?php if (have_rows('icgm_items')): ?>
				<div class="<?php echo $gridClass; ?> icgm-wrap">

					<?php while (have_rows('icgm_items')): the_row(); ?>
					<div class="icgm-item">
						<?php 
						$icgm_image = get_sub_field('icgm_img');
						$icgm_img_title = get_sub_field('icgm_heading') ? get_sub_field('icgm_heading') : $icgm_image['alt'];
						if( !empty( $icgm_image ) ): ?>
							<?php 
							$link = get_sub_field('icgm_link');
							if( $link ): 
							    $link_url = $link['url'];
							    $link_title = $link['title'] ? $link['title'] : 'Learn More';
							    $link_target = $link['target'] ? $link['target'] : '_self';
							    ?>
							    <a class="icgm-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
							<?php else: ?>
								<a href="<?php echo esc_url($icgm_image['url' ]); ?>" class="icgm-link lightbox">
							<?php endif;?>
						    	<img class="icgm-img" src="<?php echo esc_url($icgm_image['url' ]); ?>" alt="<?php echo esc_attr($icgm_img_title); ?>" title="<?php echo esc_attr($icgm_img_title); ?>" />
						    <?php //if (get_sub_field('icgm_link')): ?>
								</a>
							<?php //endif ?>
						<?php endif; ?>

						<?php if (get_sub_field('icgm_heading')): ?>
							<h3 class="icgm-heading">
								<?php if ($link): ?>
									<a class="icgm-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
								<?php endif ?>
									<?php echo get_sub_field('icgm_heading') ?>
								<?php if (get_sub_field('icgm_link')): ?>
								</a>
							<?php endif ?>	
							</h3>
						<?php endif ?>

						<?php if (get_sub_field('icgm_description')): ?>
							<p class="icgm-description"><?php echo get_sub_field('icgm_description') ?></p>
						<?php endif ?>

						<?php if ($link): ?>
							<a class="icgm-btn btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_url( $link_title ); ?> ?></a>
						<?php endif ?>
					</div>
					<?php endwhile ?>

				</div>
				<?php endif ?>
			</div>
		</section>


	<?php elseif( get_row_layout() == 'internal_page_cta_module' ): ?>    
	
      <section class="internal-page-cta-module">
        <div class="inner-wrap">
          <?php if( get_sub_field('ipcm_heading')): ?>
          <h2 class="hfwc-heading"><?php echo get_sub_field('ipcm_heading'); ?></h2>
          <?php endif; ?>	
          <div class="hfwc-cta">
          	<?php
			$ipcm_cta_one = get_sub_field('ipcm_cta_one');
			if($ipcm_cta_one):
			$link_url = $ipcm_cta_one['url'];
			$link_title = $ipcm_cta_one['title'];
			$link_target = $ipcm_cta_one['target'] ? $ipcm_cta_one['target'] : '_self';
			?>
            <a class="btn" href="<?php echo esc_url($link_url); ?>"><span><?php echo esc_html($link_title); ?></span></a>
            <?php endif; ?>

            <?php
			$ipcm_cta_two = get_sub_field('ipcm_cta_two');
			if($ipcm_cta_two):
			$link_url = $ipcm_cta_two['url'];
			$link_title = $ipcm_cta_two['title'];
			$link_target = $ipcm_cta_two['target'] ? $ipcm_cta_two['target'] : '_self';
			?>
            <a class="btn-alt" href="<?php echo esc_url($link_url); ?>"><span><?php echo esc_html($link_title); ?></span></a>
            <?php endif; ?>

          </div>
        </div>
      </section>


      <?php elseif( get_row_layout() == 'pillar_page_nav_module' ): ?>    
      <section class="internal-links-nav">
      	<div class="inner-wrap">
			<div class="isn-wrap">
				<?php if( have_rows('isn_link_wrap') ): ?>
				<ul class="internal-links-wrap">
					<?php while ( have_rows('isn_link_wrap') ) : the_row(); ?>

					<li>
						<?php $link = get_sub_field('isn_add_link');
						if( $link ): 
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						    ?>
						    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" class="super-smooth"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>	
					</li>
					<?php endwhile; ?>
				</ul>
				<?php endif; ?>
			</div>
      	</div>
		</section>	

		<?php elseif( get_row_layout() == 'destination_bucket_page_module' ): ?> 
		<section class="destination-bucket-page-module">
			<div class="inner-wrap">
				<div class="dbpm-buckets-wrap">
					<?php if( have_rows('dbpm_buckets') ): while ( have_rows('dbpm_buckets') ) : the_row(); ?>
						<div class="dbpm-buckets">
							<?php if(get_sub_field('dbpm_image')) : ?>
							<?php $dbpm_image = get_sub_field('dbpm_image'); ?>
							<div class="dbpm-image">
							   	<img src="<?php echo $dbpm_image['url']; ?>" alt="<?php echo $dbpm_image['title']; ?>" title="<?php echo $dbpm_image['title']; ?>">
							</div>
							<?php endif; ?>
							<div class="dbpm-content">
								<?php if( get_sub_field('dbpm_title')): ?>
						        <h2 class="dbpm-heading"><?php echo get_sub_field('dbpm_title'); ?></h2>
						        <?php endif; ?>

						        <?php if( get_sub_field('dbpm_desc')): ?>
						        <div><?php echo get_sub_field('dbpm_desc'); ?></div>
						        <?php endif; ?>

						        <?php
								$dbpm_link = get_sub_field('dbpm_link');
								if($dbpm_link):
								$link_url = $dbpm_link['url'];
								$link_title = $dbpm_link['title'];
								$link_target = $dbpm_link['target'] ? $dbpm_link['target'] : '_self';
								?>
								<div class="dbpm-cta-wrap">
					            	<a class="btn" href="<?php echo esc_url($link_url); ?>"><span><?php echo esc_html($link_title); ?></span></a>
								</div>
					            <?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>	
					<?php endif; ?>
				</div>
			</div>
		</section>   

<?php endif; ?>
<?php endwhile; echo '</section>'; ?>
<?php endif; ?>




