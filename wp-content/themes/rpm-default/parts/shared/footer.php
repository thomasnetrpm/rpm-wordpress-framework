

<!--Site Footer -->
<footer class="site-footer" role="contentinfo">
	<p class="sf-copy">© <?php echo date("Y"); ?> <a class="sf-comp-copy" href="<?php bloginfo('url'); ?>"><?php bloginfo( 'name' ); ?></a>, All Rights Reserved <span>|</span> Site created by <a href="https://business.thomasnet.com/marketing-services" target="_blank" rel="noreferrer noopener">Thomas Marketing Services</a></p>
</footer>

